const express = require('express');
const router = express.Router();

const barangController = require('../controllers/barangController');

/**
 * @swagger
 * /users/users:
 *   get:
 *     description: Mendapatkan daftar semua pengguna
 *     responses:
 *       200:
 *         description: Sukses mendapatkan daftar pengguna
 */
router.get('/users', barangController.getBarang);

/**
 * @swagger
 * /users/create:
 *   post:
 *     summary: Create a new user
 *     description: Create a new user with the provided details.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - nama_barang
 *               - stok
 *               - jumlah_terjual
 *               - tanggal_transaksi
 *               - jenis_barang
 *             properties:
 *               nama_barang:
 *                 type: string
 *               stok:
 *                 type: string
 *               jumlah_terjual:
 *                 type: string
 *               tanggal_transaksi:
 *                 type: string
 *               jenis_barang:
 *                 type: string
 *     responses:
 *       201:
 *         description: User created successfully
 *       400:
 *         description: Bad request
 *       500:
 *         description: Internal server error
 */
router.post('/create', barangController.createBarang);

/**
 * @swagger
 * /users/barang/{id}:
 *   put:
 *     summary: Update a barang by ID
 *     description: Update a barang with the provided ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: ID of the barang to update
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               nama_barang:
 *                 type: string
 *               stok:
 *                 type: string
 *               jumlah_terjual:
 *                 type: string
 *               tanggal_transaksi:
 *                 type: string
 *               jenis_barang:
 *                 type: string
 *     responses:
 *       200:
 *         description: Barang updated successfully
 *       400:
 *         description: Bad request
 *       404:
 *         description: Barang not found
 *       500:
 *         description: Internal server error
 */
router.put('/barang/:id', barangController.updateBarang);

/**
 * @swagger
 * /users/detail/barang/{id}:
 *   get:
 *     summary: Get a barang by ID
 *     description: Retrieve a barang with the provided ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: ID of the barang to retrieve
 *     responses:
 *       200:
 *         description: Barang retrieved successfully
 *       404:
 *         description: Barang not found
 *       500:
 *         description: Internal server error
 */
router.get('/detail/barang/:id', barangController.getBarangById);

/**
 * @swagger
 * /users/delete/barang/{id}:
 *   delete:
 *     summary: Delete a barang by ID
 *     description: Delete a barang with the provided ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: ID of the barang to delete
 *     responses:
 *       200:
 *         description: Barang deleted successfully
 *       404:
 *         description: Barang not found
 *       500:
 *         description: Internal server error
 */
router.delete('/delete/barang/:id', barangController.deleteById);

module.exports = router;

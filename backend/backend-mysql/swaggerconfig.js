const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const options = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            title: 'API Dokumentasi',
            version: '1.0.0',
            description: 'Deskripsi API Anda',
        },
    },
    apis: ['./routes/barangRoutes.js'],
};

const specs = swaggerJsdoc(options);

module.exports = { specs, swaggerUi };

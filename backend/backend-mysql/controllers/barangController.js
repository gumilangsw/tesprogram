const barangModel = require('../models/barangModel');

exports.getBarang = async (req, res) => {
    try {
        const users = await barangModel.getBarang();
        res.json(users);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

exports.createBarang = async (req, res) => {
    const {
        nama_barang,
        stok,
        jumlah_terjual,
        tanggal_transaksi,
        jenis_barang
    } = req.body;

    try {
        const newUser = await barangModel.createBarang({
            nama_barang,
            stok,
            jumlah_terjual,
            tanggal_transaksi,
            jenis_barang
        });
        res.status(201).json(newUser);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
};

exports.updateBarang = async (req, res) => {
    const { id } = req.params;
    const { nama_barang, stok, jumlah_terjual, tanggal_transaksi, jenis_barang } = req.body;

    try {
        const result = await barangModel.updateBarang(id, {
            nama_barang,
            stok,
            jumlah_terjual,
            tanggal_transaksi,
            jenis_barang
        });

        if (result.affectedRows === 0) {
            return res.status(404).json({ message: 'Barang not found' });
        }

        res.json({ message: 'Barang updated successfully' });
    } catch (error) {
        res.status(500).json({ message: 'Error updating barang', error });
    }
};

exports.getBarangById = async (req, res) => {
    try {
        const id = req.params.id;
        const rows = await barangModel.getBarangById(id);
        if (rows.length > 0) {
            res.status(200).json(rows[0]);
        } else {
            res.status(404).json({ message: 'Barang not found' });
        }
    } catch (error) {
        res.status(500).json({ message: 'Server error', error });
    }
};
exports.deleteById = async (req, res) => {
    try {
        const id = req.params.id;
        const result = await barangModel.deleteById(id);
        if (result.affectedRows > 0) {
            res.status(200).json({ message: 'Barang deleted successfully' });
        } else {
            res.status(404).json({ message: 'Barang not found' });
        }
    } catch (error) {
        res.status(500).json({ message: 'Server error', error });
    }
};
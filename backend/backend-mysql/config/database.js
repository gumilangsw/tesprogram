const mysql = require('mysql2');

// Buat koneksi ke database
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'barang'
});

module.exports = pool.promise();

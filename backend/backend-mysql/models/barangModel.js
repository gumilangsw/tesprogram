const pool = require('../config/database');

const getBarang = async () => {
    const [rows] = await pool.query('SELECT * FROM barang');
    return rows;
};

const createBarang = async ({
    nama_barang,
    stok,
    jumlah_terjual,
    tanggal_transaksi,
    jenis_barang
}) => {
    try {
        const [result] = await pool.query(
            'INSERT INTO barang (nama_barang, stok, jumlah_terjual, tanggal_transaksi, jenis_barang) VALUES (?, ?, ?, ?, ?)',
            [nama_barang, stok, jumlah_terjual, tanggal_transaksi, jenis_barang]
        );
        return {
            id: result.insertId, // Mengakses ID yang dibuat secara otomatis
            affectedRows: result.affectedRows
        };
    } catch (error) {
        console.error('Error creating user:', error);
        throw error;
    }
};
const updateBarang = async (id, data) => {
    const { nama_barang, stok, jumlah_terjual, tanggal_transaksi, jenis_barang } = data;
    try {
        const [result] = await pool.query(
            'UPDATE barang SET nama_barang = ?, stok = ?, jumlah_terjual = ?, tanggal_transaksi = ?, jenis_barang = ? WHERE id = ?',
            [nama_barang, stok, jumlah_terjual, tanggal_transaksi, jenis_barang, id]
        );
        return {
            affectedRows: result.affectedRows
        };
    } catch (error) {
        console.error('Error updating barang:', error);
        throw error;
    }
};

const getBarangById = async (id) => {
    const [rows] = await pool.query('SELECT * FROM barang WHERE id = ?', [id]);
    return rows;
};
const deleteById = async (id) => {
    const [result] = await pool.query('DELETE FROM barang WHERE id = ?', [id]);
    return result;
};
module.exports = {
    getBarang,
    createBarang,
    updateBarang,
    getBarangById,
    deleteById
};
